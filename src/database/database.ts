import {Venue} from "../entities/venue";
import {Region} from "../entities/region";
import {Event} from "../entities/event";
import {URL} from 'url';

import {Connection, createConnection, getConnection, getRepository} from 'typeorm';
import {EventVersion} from "../entities/eventVersion";
import {ConnectionOptions} from "typeorm/connection/ConnectionOptions";
import {VenueAlias} from "../entities/venueAlias";
import {GenreCategory} from "../entities/genreCategory";
import {Genre} from "../entities/genre";
import {User} from "../entities/user";
import {Logger} from "../logger/logger";
import {TypeormLogger} from "../logger/typeormLogger";

async function createPostgresConnection(connectionString?: string, options?: Object): Promise<Connection> {
    let host: string = 'localhost';
    let port: number = 5432;
    let username: string = 'postgres';
    let password: string = 'postgres';
    let database: string = 'postgres';

    if (connectionString) {
        const url = new URL(connectionString);
        host = url.hostname;
        port = parseInt(url.port);
        username = url.username;
        password = url.password;
        database = url.pathname && url.pathname.substr(1);
    }
    let databaseOptions : ConnectionOptions = {
        type: 'postgres',
        host: host,
        port: port,
        username: username,
        password: password,
        database: database,
        extra: {
            max: 50,
        },
        entities: [
            Venue,
            Region,
            Event,
            EventVersion,
            VenueAlias,
            GenreCategory,
            Genre,
            User
        ],
        synchronize: false,
        migrationsRun: true,
        migrationsTableName: "migrations",
        migrations: ["src/migration/*.ts"],
        cli: {
            migrationsDir: "src/migration"
        }
    };
    if (options) {
        Object.assign(databaseOptions, options);
    }
    return await createConnection(databaseOptions);
}

async function truncateAllTables() {
    let connection = getConnection();
    const entities = [];
    (await (connection.entityMetadatas).forEach(
        x => entities.push({name: x.name, tableName: x.tableName})
    ));
    for (const entity of entities) {
        await connection.query(`TRUNCATE TABLE "${entity.tableName}" CASCADE`);
    }
}

async function connectDatabase(logger: Logger) {
    let url = `postgres://postgres:postgres@localhost:5432/postgres`;
    if (process.env.RDS_POSTGRES_URL) {
        url = process.env.RDS_POSTGRES_URL;
    } else if (process.env.DATABASE_HOST) {
        url = `postgres://postgres:postgres@${process.env.DATABASE_HOST}:5432/postgres`;
    }
    await createPostgresConnection(url, {
        logger: new TypeormLogger(logger),
        logging: "all"
    });
}

async function connectDatabaseTests() {
    let url = `postgres://postgres:postgres@localhost:5433/postgres`;
    if (process.env.DATABASE_HOST_TEST) {
        url = `postgres://postgres:postgres@${process.env.DATABASE_HOST_TEST}:5432/postgres`;
    }
    await createPostgresConnection(url, {
        dropSchema: true,
        logging: ['error']
    });
    await truncateAllTables();
}

export {connectDatabase, connectDatabaseTests, truncateAllTables};
