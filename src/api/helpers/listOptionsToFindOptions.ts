export interface FieldMap {
    [key: string]: string
}

export const queryBuilderWithListOptions = function (repository, alias, listOptions, lowercaseSort: String[] = [], fieldMap: FieldMap = {}) {
    const queryBuilder = repository.createQueryBuilder(alias);

    if (listOptions && listOptions.sort) {
        let field = "";
        if (fieldMap[listOptions.sort.field] !== undefined) {
            field = fieldMap[listOptions.sort.field];
        } else {
            field = alias + "." + listOptions.sort.field;
        }
        if (lowercaseSort.includes(listOptions.sort.field)) {
            queryBuilder.addSelect(`LOWER(${field})`, "lowercasesort")
            field = `lowercasesort`;
        }
        const ascending = listOptions.sort.ascending !== false;

        queryBuilder.addOrderBy(field, ascending ? "ASC" : "DESC");
    }

    if (listOptions && listOptions.pagination) {
        let take = listOptions.pagination.perPage;
        const page = listOptions.pagination.page;
        let skip = 0;
        if (page > 1) {
            skip = (page - 1) * take;
        }
        queryBuilder.take(take).skip(skip);
    }

    return queryBuilder;
}