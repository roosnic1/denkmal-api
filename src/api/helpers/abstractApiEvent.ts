import moment = require("moment-timezone");
import {Genre} from "../../entities/genre";

export abstract class AbstractApiEvent {
    id: string;
    createdAt: moment.Moment;
    from: moment.Moment;
    until: moment.Moment | null;
    description: string;
    isReviewPending: boolean;
    hasTime: boolean;
    links: [{label: string, url: string}];
    genres: Genre[];
}