import * as bcrypt from 'bcryptjs';

const saltRounds = 10;

export const hashPassword = (password : string) : string => bcrypt.hashSync(password, saltRounds);

export const checkPassword = (password : string, hash : string) : Boolean => bcrypt.compareSync(password, hash);