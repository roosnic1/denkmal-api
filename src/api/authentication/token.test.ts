import {connectDatabaseTests} from "../../database/database";
import {getConnection, getManager} from "typeorm";
import {User} from "../../entities/user";
import {createToken, getUserWithToken} from "./token";

describe("test auth tokens", async () => {
    const user: User = null

    beforeEach(async () => {
        await connectDatabaseTests();

        const entityManager = getManager();

        const user = new User();
        user.name = "test"
        user.email = "test@test.com"
        user.password = "password"
        this.user = await entityManager.save(user);
    })

    afterEach(async () => {
        await getConnection().close();
    });

    test('create and verify', async() => {
        const token = await createToken(this.user)
        expect(token).toBeTruthy()

        const foundUser = await getUserWithToken(token)
        expect(foundUser).toBeDefined()
        expect(foundUser.id).toEqual(this.user.id)
    })
})