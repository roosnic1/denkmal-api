import {getCustomRepository, getManager, getRepository} from "typeorm";
import {RegionRepository} from "repository/regionRepository";
import {VenueRepository} from "../../../repository/venueRepository";
import {Region} from "../../../entities/region";
import {Venue} from "../../../entities/venue";
import {EventVersionRepository} from "../../../repository/eventVersionRepository";
import {EventRepository} from "../../../repository/eventRepository";
import {Event} from "../../../entities/event";
import {EventVersion, SourceTypeEnum} from "../../../entities/eventVersion";
import {Genre} from "../../../entities/genre";
import {GenreCategory, unknownGenreCategoryName} from "../../../entities/genreCategory";

function isValid(region: Region, attrs): Boolean {
    let todaysDateRange = region.getEventDayRangeNow();
    if (todaysDateRange[0].isAfter(attrs.from)
        || todaysDateRange[0].clone().add(10, 'days').isBefore(attrs.from)) {
        return false;
    }
    if (attrs.until && attrs.until.isSameOrBefore(attrs.from)) {
        return false;
    }

    return attrs.venueName.trim() != '';
}

async function makeEventVersionActiveIfAppropriate(eventVersion: EventVersion) : Promise<any> {
    const event = await eventVersion.event;
    const eventRepository = getCustomRepository(EventRepository);

    const makeActive = async () => {
        return await eventRepository
            .createQueryBuilder()
            .update(Event)
            .set({ activeVersion: eventVersion })
            .where("id = :id", { id: event.id })
            .execute();
    }

    if (!event.activeVersion) {
        return await makeActive();
    }

    if (event.activeVersion.isReviewPending) {
        if (eventVersion.sourcePriority > event.activeVersion.sourcePriority) {
            return await makeActive();
        }
    }

    return Promise.resolve(event);
}

async function addScraperEvent(scraperEvent, genreIdsCache, unknownGenreCategoriesCache, region: Region, exactDateMatching : Boolean = false) {
    if (!isValid(region, scraperEvent)) {
        return false;
    }

    if (!genreIdsCache[region.id]) {
        let genreIds = {};
        region.genreCategories.forEach(c => {
            c.genres.forEach(g => {
                genreIds[g.name] = g.id;
            });
        });
        genreIdsCache[region.id] = genreIds;
    }
    if (!unknownGenreCategoriesCache[region.id]) {
        let unknownGenreCategory = await region.genreCategories.find(c => c.name == unknownGenreCategoryName);
        if (!unknownGenreCategory) {
            let category = new GenreCategory();
            category.name = unknownGenreCategoryName;
            category.region = Promise.resolve(region);
            unknownGenreCategory = await getRepository(GenreCategory).save(category);
        }

        unknownGenreCategoriesCache[region.id] = unknownGenreCategory;
    }

    const addGenres = async function (genreStrings: string[]) {
        if (!genreStrings) {
            return;
        }
        const genreRepository = getRepository(Genre);
        for (const genreName of genreStrings) {
            const lowerCaseName = genreName.toLowerCase();
            if (!genreIdsCache[region.id][lowerCaseName]) {
                const genre = new Genre();
                genre.name = lowerCaseName;
                genre.category = unknownGenreCategoriesCache[region.id];
                await genreRepository.save(genre);
                genreIdsCache[region.id][lowerCaseName] = genre.id;
            }
        }
    };

    const venueRepository = getCustomRepository(VenueRepository);
    let venue = await venueRepository.findByNameOrAlias(region, scraperEvent.venueName);

    if (venue) {
        if (venue.ignoreScraper) {
            return false;
        }
    } else {
        // create venue
        venue = await getManager().save(Object.assign(new Venue(), {
            name: scraperEvent.venueName,
            isReviewPending: true,
            region: Promise.resolve(region)
        }));
    }

    let genresRelations = [];

    if (Array.isArray(scraperEvent.genres)) {
        await addGenres(scraperEvent.genres);
        genresRelations = scraperEvent.genres.map(name => {return {id: genreIdsCache[region.id][name.toLowerCase()]}})
    }

    const eventRepository = getCustomRepository(EventRepository);
    const eventVersionRepository = getCustomRepository(EventVersionRepository);

    // check if event already exists
    let similar = await eventRepository.findSimilar(venue.id, scraperEvent.from, exactDateMatching);
    let event = null;
    const exists = similar && similar.length > 0;
    if (exists) {
        event = similar[0];

        // check for version of same identifier
        const versions = await event.versions;
        const sameSourceVersions = versions.filter(e => e.sourceIdentifier == scraperEvent.sourceIdentifier);
        if (sameSourceVersions.length == 0) {
            // create version
            let newVersion = Object.assign(
                new EventVersion(),
                {
                    ...scraperEvent,
                    sourceType: SourceTypeEnum.Scraper,
                    isReviewPending: true,
                    event: Promise.resolve(event),
                    genres: genresRelations
                }
            );

            if (event.activeVersion.isSame(newVersion)) {
                // already reviewed this data
                // we still save a new version in that case since we don't have a version yet from this scraper
                newVersion.isReviewPending = false;
            }

            newVersion = await eventVersionRepository.save(newVersion);

            await makeEventVersionActiveIfAppropriate(newVersion);

            return true;
        } else {
            // we already have a version from this scraper
            const versionsByDate = sameSourceVersions.sort((a, b) => {
                return b.createdAt.unix() - a.createdAt.unix()
            });

            const latestVersion = versionsByDate[0];

            let newVersion : EventVersion = null;
            if (latestVersion.isReviewPending) {
                // update the version
                newVersion = await eventVersionRepository.save({
                    isReviewPending: true,
                    sourceType: SourceTypeEnum.Scraper,
                    ...latestVersion,
                    ...scraperEvent,
                    event: Promise.resolve(event),
                    genres: genresRelations
                });
            } else {
                // check for changes
                newVersion = Object.assign(new EventVersion(), {
                    ...scraperEvent,
                    genres: genresRelations
                });
                if (newVersion.isSame(latestVersion)) {
                    // ignore when no changes detected
                } else {
                    newVersion = await eventVersionRepository.save(Object.assign(newVersion, {
                        sourceType: SourceTypeEnum.Scraper,
                        event: Promise.resolve(event),
                        isReviewPending: true,
                    }));
                }
            }

            await makeEventVersionActiveIfAppropriate(newVersion);

            return true;
        }
    } else {
        // create new hidden event and version
        event = await eventRepository.save(Object.assign(new Event(), {
            isHidden: true,
            venue: Promise.resolve(venue)
        }));

        const version = await eventVersionRepository.save(Object.assign(new EventVersion(), {
            ...scraperEvent,
            sourceType: SourceTypeEnum.Scraper,
            isReviewPending: true,
            event: Promise.resolve(event),
            genres: genresRelations
        }));


        await makeEventVersionActiveIfAppropriate(version);

        return true;
    }
}

export const addScraperEventsMutation = async function (_, attrs) {
    const regionRepository = getCustomRepository(RegionRepository);
    const regions = await regionRepository.find({relations: ["genreCategories", "genreCategories.genres"]});
    const unknownGenreCategoriesCache = {};
    const genreIdsCache = {};

    // count events from a region + source + venue + day
    const eventCount = new Map();

    let enhancedEvents = [];
    for (let scraperEvent of attrs.events) {
        const region = regions.find(r => r.id === scraperEvent.regionId);
        if (!region) {
            throw new Error(`Cannot find region with ID '${scraperEvent.regionId}'`);
        }

        const eventDay = region.getEventDay(scraperEvent.from);

        const hash = region.id + scraperEvent.venueName + eventDay + scraperEvent.sourceIdentifier;

        let count = eventCount.get(hash);
        if (!count) {
            count = 1;
        } else {
            count += 1;
        }
        eventCount.set(hash, count);

        enhancedEvents.push({
            ...scraperEvent,
            region: region,
            eventDay: eventDay,
            hash: hash
        });
    }

    let returnArray = [];
    for (let scraperEvent of enhancedEvents) {
        let count = eventCount.get(scraperEvent.hash);
        // in case of multiple events on the same day for the same region/venue/source, we want to actively create multiple events
        let exactDateMatching = count > 1;
        returnArray.push(await addScraperEvent(scraperEvent, genreIdsCache, unknownGenreCategoriesCache, scraperEvent.region, exactDateMatching));
    }
    return returnArray;
};
