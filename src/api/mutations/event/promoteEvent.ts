import {getCustomRepository} from "typeorm";
import {EventRepository} from "repository/eventRepository";
import {ApiEvent} from "api/helpers/apiEvent";

export const promoteEventMutation = async function (_, attrs) {
    const eventRepository = getCustomRepository(EventRepository);
    const event = await eventRepository.findOneOrFail(attrs.id);

    event.isPromoted = attrs.promote == true;

    await eventRepository.save(event);

    return ApiEvent.apiEventForEvent(event);
};