import {getCustomRepository} from 'typeorm';
import {EventVersionRepository} from "repository/eventVersionRepository";
import {EventRepository} from "repository/eventRepository";
import {EventVersion, SourceTypeEnum} from "entities/eventVersion";
import {ApiEvent} from "api/helpers/apiEvent";
import {VenueRepository} from "../../../repository/venueRepository";


export const reviewEventMutation = async function (_, attrs) {
    /*
        creates new version
     */
    const eventRepository = getCustomRepository(EventRepository);
    const event = await eventRepository.findOneOrFail(attrs.id);

    if (attrs.venueId) {
        const venue = await getCustomRepository(VenueRepository).findOneOrFail(attrs.venueId);
        event.venue = Promise.resolve(venue);
    }

    const eventVersion = event.activeVersion;

    const eventVersionRepository = getCustomRepository(EventVersionRepository);

    let genresObject = {};
    if (attrs.genres) {
        genresObject = {
            genres: attrs.genres.map(genreId => {return {id: genreId}})
        };
    }

    const newEventVersion = await eventVersionRepository.save(Object.assign(
        new EventVersion(),
        {
            ...eventVersion,
            sourceType: SourceTypeEnum.Admin,
            event: Promise.resolve(event),
            isReviewPending: false
        },
        {
            ...attrs
        },
        genresObject
    ));

    // mark all versions as reviewed
    for (let v of await event.versions) {
        await eventVersionRepository.save(Object.assign(v, {
            isReviewPending: false
        }));
    }

    // activate created version
    event.activeVersion = newEventVersion;
    if (attrs.isPromoted !== undefined) {
        event.isPromoted = attrs.isPromoted;
    }
    if (attrs.isHidden !== undefined) {
        event.isHidden = attrs.isHidden;
    }
    await eventRepository.save(event);

    // get event with all eager loaded data
    const query = eventRepository.createQueryBuilder("event")
        .where({id: event.id})
        .innerJoinAndSelect("event.activeVersion", "activeVersion")
        .leftJoinAndSelect("activeVersion.genres", "genres")
        .leftJoinAndSelect("genres.category", "category");

    return ApiEvent.apiEventForEvent(await query.getOne());
};
