import {getManager} from "typeorm";
import {Region} from "../../../entities/region";

export const deleteRegionMutation = async function (_, attrs) {
    const entityManager = getManager();
    const regionId = attrs.id;
    await entityManager.findOneOrFail(Region, {id: regionId});

    return entityManager.delete(Region, {id: regionId});
};