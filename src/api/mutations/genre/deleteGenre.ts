import {getManager} from "typeorm";
import {Genre} from "../../../entities/genre";

export const deleteGenreMutation = async function (_, attrs) {
    const entityManager = getManager();
    const genreId = attrs.id;
    await entityManager.delete(Genre, {id: genreId});
    return true;
};