import {getManager} from 'typeorm';
import {Genre} from "../../../entities/genre";
import {GenreCategory} from "../../../entities/genreCategory";


export const addGenreMutation = async function (_, attrs) {
    const entityManager = getManager();

    const genre = {
        ...attrs,
        isReviewPending: false,
        category: await entityManager.findOneOrFail(GenreCategory, {id: attrs.genreCategoryId})
    }

    return entityManager.save(Object.assign(new Genre(), genre));
};
