import {getManager} from 'typeorm';
import {Genre} from "../../../entities/genre";
import {GenreCategory} from "../../../entities/genreCategory";

export const changeGenreMutation = async function (_, attrs) {
    const entityManager = getManager();
    const genre = await entityManager.findOneOrFail(Genre, {id: attrs.id});

    const entity = {
        id: genre.id,
        ...attrs,
        isReviewPending: false
    };
    if (attrs.genreCategoryId) {
        entity.category = await entityManager.findOneOrFail(GenreCategory, {id: attrs.genreCategoryId})
    }

    await entityManager.save(Genre, entity);

    return await entityManager.findOneOrFail(Genre, {id: attrs.id}, {relations: ["category"]});
};
