import {getManager} from 'typeorm';
import {GenreCategory} from "../../../entities/genreCategory";

export const changeGenreCategoryMutation = async function (_, attrs) {
    const entityManager = getManager();
    const genreCategory = await entityManager.findOneOrFail(GenreCategory, {id: attrs.id});

    return await entityManager.save(GenreCategory, {
        id: genreCategory.id,
        ...attrs
    });
};
