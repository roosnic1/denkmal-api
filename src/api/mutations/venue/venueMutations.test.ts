import {gql} from "apollo-server-koa";
import {getConnection} from "typeorm";
import {createTestServer} from "../../../test/testServer";
import {AccessLevelEnum} from "../../../entities/user";

describe("venue mutations test", async () => {
    let query, mutate;
    let regionId;

    beforeEach(async () => {
        ({query, mutate} = await createTestServer(AccessLevelEnum.Admin));

        let res1 = await mutate({
            mutation: gql`
                mutation {addRegion(
                    name: "MyRegion-1"
                    slug: "venueMutations-region-1"
                    latitude: 40
                    longitude: 50
                    timeZone: "Europe/Zurich"
                    dayOffset: 0
                    email: "mail@asdf.ch"
                ){id}}
            `
        });
        regionId = res1.data.addRegion.id;
    });

    afterEach(async () => {
        await getConnection().close();
    });


    test("Test addVenue and addVenueAlias", async () => {
        const venue1Name = "my venue";
        const venue1Aliases = ["an alias", "another one"];

        const venue1Result = await mutate({
            mutation: gql`
                mutation {addVenue(
                    regionId: "${regionId}"
                    name: "${venue1Name}"
                ){id}}
            `
        });
        const venue1Id = venue1Result.data.addVenue.id;
        expect(venue1Id).toBeDefined();

        await mutate({
            mutation: gql`
                mutation {changeVenue(
                    id: "${venue1Id}"
                    aliases: ["${venue1Aliases[0]}","${venue1Aliases[1]}"]
                ){id}}
            `
        });

        const query1 = await query({
            query: gql`
                query {
                    venue(id: "${venue1Id}") {
                        name
                        aliases {name}
                    }
                }
            `
        });

        expect(query1.data.venue.name).toEqual(venue1Name);
        expect(query1.data.venue.aliases.length).toEqual(2);
        expect(query1.data.venue.aliases.find(a => a.name == venue1Aliases[0])).toBeTruthy();
        expect(query1.data.venue.aliases.find(a => a.name === venue1Aliases[1])).toBeTruthy();
    });
});