import {getManager} from "typeorm";
import {Venue} from "entities/venue";

export const deleteVenueMutation = async function (_, attrs) {
    const entityManager = getManager();
    const venueId = attrs.id;
    await entityManager.delete(Venue, {id: venueId});
    return true;
};