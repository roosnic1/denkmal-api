import {eventResolver} from './resolvers/event/eventResolver';
import {eventsResolver} from './resolvers/event/eventsResolver';
import {addRegionMutation} from "./mutations/region/addRegion";
import {regionResolver} from "./resolvers/region/regionResolver";
import {regionsResolver} from "./resolvers/region/regionsResolver";
import {changeRegionMutation} from "./mutations/region/changeRegion";
import {addVenueMutation} from "./mutations/venue/addVenue";
import {venueResolver} from "./resolvers/venue/venueResolver";
import {changeVenueMutation} from "./mutations/venue/changeVenue";
import {reviewEventMutation} from "./mutations/event/reviewEvent";
import {eventDayResolver} from "./resolvers/event/eventDayResolver";
import {dateResolver} from "./resolvers/dateResolver";
import {suggestEventMutation} from "./mutations/event/suggestEvent";
import {hideEventMutation} from "./mutations/event/hideEvent";
import {deleteVenueMutation} from "./mutations/venue/deleteVenue";
import {deleteRegionMutation} from "./mutations/region/deleteRegion";
import {addScraperEventsMutation} from "./mutations/event/addScraperEvents";
import {addGenreCategoryMutation} from "./mutations/genre/addGenreCategory";
import {addGenreMutation} from "./mutations/genre/addGenre";
import {urlResolver} from "./resolvers/urlResolver";
import {promoteEventMutation} from "./mutations/event/promoteEvent";
import {venuesListResolver} from "./resolvers/venue/venuesListResolver";
import {eventsListResolver} from "./resolvers/event/eventsListResolver";
import {regionsListResolver} from "./resolvers/region/regionsListResolver";
import {addEventMutation} from "./mutations/event/addEvent";
import {authenticateResolver} from "./resolvers/user/authenticateResolver";
import {passwordResolver} from "./resolvers/passwordResolver";
import {addUserMutation} from "./mutations/user/addUser";
import {changeUserMutation} from "./mutations/user/changeUser";
import {deleteUserMutation} from "./mutations/user/deleteUser";
import {restrictAccess} from "./authentication/resolverDecorators";
import {AccessLevelEnum} from "../entities/user";
import {genreCategoriesListResolver} from "./resolvers/genre/genreCategoriesListResolver";
import {genreCategoryResolver} from "./resolvers/genre/genreCategoryResolver";
import {deleteGenreCategoryMutation} from "./mutations/genre/deleteGenreCategory";
import {deleteGenreMutation} from "./mutations/genre/deleteGenre";
import {changeGenreCategoryMutation} from "./mutations/genre/changeGenreCategory";
import {genresListResolver} from "./resolvers/genre/genresListResolver";
import {changeGenreMutation} from "./mutations/genre/changeGenre";
import {genreResolver} from "./resolvers/genre/genreResolver";
import {genresResolver} from "./resolvers/genre/genresResolver";
import {mergeVenueMutation} from "./mutations/venue/mergeVenue";
import {usersListResolver} from "./resolvers/user/usersListResolver";
import {userResolver} from "./resolvers/user/userResolver";
import {badgesResolver} from "./resolvers/region/badgesResolver";

export const resolvers = {
    EventDay: eventDayResolver,
    URL: urlResolver,
    Date: dateResolver,
    Password: passwordResolver,
    Query: {
        event: eventResolver,
        region: regionResolver,
        regions: regionsResolver,
        regionsList: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], regionsListResolver),
        venue: venueResolver,
        authenticate: authenticateResolver,
        genreCategory: genreCategoryResolver,
        genre: genreResolver,
        genres: genresResolver,
        usersList: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], usersListResolver),
        user: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], userResolver)
    },
    Venue: {
        events: eventsResolver,
    },
    Region: {
        events: eventsResolver,
        venuesList: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], venuesListResolver),
        eventsList: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], eventsListResolver),
        genresList: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], genresListResolver),
        genreCategoriesList: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], genreCategoriesListResolver),
        badges: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], badgesResolver)
    },
    Mutation: {
        // regions
        addRegion: restrictAccess(AccessLevelEnum.Admin, addRegionMutation),
        changeRegion: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], changeRegionMutation),
        deleteRegion: restrictAccess(AccessLevelEnum.Admin, deleteRegionMutation),

        // events
        suggestEvent: suggestEventMutation,
        addEvent: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], addEventMutation),
        reviewEvent: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], reviewEventMutation),
        addScraperEvents: restrictAccess([AccessLevelEnum.Scraper, AccessLevelEnum.Admin], addScraperEventsMutation),
        hideEvent: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], hideEventMutation),
        promoteEvent: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], promoteEventMutation),

        // genres
        addGenreCategory: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], addGenreCategoryMutation),
        changeGenreCategory: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], changeGenreCategoryMutation),
        deleteGenreCategory: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], deleteGenreCategoryMutation),
        addGenre: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], addGenreMutation),
        changeGenre: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], changeGenreMutation),
        deleteGenre: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], deleteGenreMutation),

        // venues
        addVenue: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], addVenueMutation),
        deleteVenue: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], deleteVenueMutation),
        changeVenue: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], changeVenueMutation),
        mergeVenue: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], mergeVenueMutation),

        // users
        addUser: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], addUserMutation),
        changeUser: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], changeUserMutation),
        deleteUser: restrictAccess([AccessLevelEnum.Admin, AccessLevelEnum.Regional], deleteUserMutation),

    },
};
