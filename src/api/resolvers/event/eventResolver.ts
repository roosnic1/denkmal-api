import {getCustomRepository} from "typeorm";
import {ApiEvent} from "api/helpers/apiEvent";
import {EventRepository} from "repository/eventRepository";

export const eventResolver = async function(obj, { id }, context, info) {
    const repository = getCustomRepository(EventRepository);
    const query = repository.createQueryBuilder("event")
        .where({id: id})
        .innerJoinAndSelect("event.activeVersion", "activeVersion")
        .leftJoinAndSelect("activeVersion.genres", "genres")
        .leftJoinAndSelect("genres.category", "category");
    const event = await query.getOne()

    return ApiEvent.apiEventForEvent(event);
};