import {GraphQLScalarType, Kind} from "graphql";
import moment = require("moment-timezone");


export const eventDayResolver = new GraphQLScalarType({
    name: 'EventDay',
    description: 'Date with format: YYYY-MM-DD',
    serialize(value) {
        return value;
    },
    parseValue(value) {
        let m = moment(value, "YYYY-MM-DD", true);
        if (m && m.isValid()) {
            return m;
        }
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                const m =  moment(ast.value as string, "YYYY-MM-DD", true);
                if (m && m.isValid()) {
                    return m;
                }
        }
    }
});