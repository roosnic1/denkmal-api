import {getCustomRepository} from "typeorm";
import {Region} from "../../../entities/region";
import {EventRepository} from "../../../repository/eventRepository";
import {
    FieldMap,
    queryBuilderWithListOptions
} from "../../helpers/listOptionsToFindOptions";
import {ApiEvent} from "../../helpers/apiEvent";

export const eventsListResolver = async function(obj, args, context, info) {
    let region = obj as Region;

    const repository = getCustomRepository(EventRepository);

    const alias = "event";


    const lowercaseStrings = ["description"];

    let listOptions = {
        ...args.listOptions
    };

    const fieldMap: FieldMap = {
        description: 'activeVersion.description',
        from: 'activeVersion.from',
        isReviewPending: "activeVersion.isReviewPending",
        "venue.name": "venue.name"
    };

    const findQuery = queryBuilderWithListOptions(repository, alias, listOptions, lowercaseStrings, fieldMap).where(alias + ".regionId = :regionId", {regionId: region.id});

    if (listOptions && listOptions.sort && listOptions.sort.field && listOptions.sort.field === 'isReviewPending') {
        findQuery.addOrderBy("activeVersion.from", "DESC");
    }

    findQuery.leftJoinAndSelect("event.activeVersion", "activeVersion");
    findQuery.leftJoinAndSelect("event.venue", "venue");
    findQuery.leftJoinAndSelect("activeVersion.genres", "genres");
    findQuery.leftJoinAndSelect("genres.category", "category");

    if (args.search && args.search.length > 0) {
        findQuery.andWhere("activeVersion.description ILIKE :search", {search: args.search});
    }

    const apiEvents = (await findQuery.getMany()).map(async e => await ApiEvent.apiEventForEvent(e));

    return {
        events: apiEvents,
        count: findQuery.getCount()
    }
};
