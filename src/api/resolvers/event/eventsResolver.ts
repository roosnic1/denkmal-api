import {getCustomRepository} from "typeorm";
import {EventRepository} from "repository/eventRepository";
import {ApiEvent} from "api/helpers/apiEvent";
import {RegionRepository} from "repository/regionRepository";
import {Region} from "entities/region";
import {Venue} from "entities/venue";

export const eventsResolver = async function(obj, args, context, info) {
    let venue: Venue = null;
    let region: Region = null;

    // TODO: this is unsafe as the constructor.name might get minified
    if (obj && obj.constructor.name == "Venue") {
        region = await getCustomRepository(RegionRepository).findOneOrFail(obj.regionId);
        venue = obj as Venue;
    }

    if (obj && obj.constructor.name == "Region") {
        region = obj as Region;
    }

    let eventDays = args.eventDays;

    if (!eventDays) {
        const [today] = region.getEventDayRangeNow();

        eventDays = [today];
        for(let i = 1; i <= 6; i++) {
            eventDays.push(today.clone().add(i, 'days'));
        }
    }

    return await Promise.all(
        eventDays.map(async eventDay => {
            const events = await getCustomRepository(EventRepository).findByEventDay(eventDay, region, venue, args.withHidden);
            return events.map(async e => await ApiEvent.apiEventForEvent(e));
        })
    );
};