import {Genre} from "../../../entities/genre";
import {getRepository} from "typeorm";

export const genreResolver = async function (obj, {id}, context, info) {
    const repository = getRepository(Genre);

    return repository.findOneOrFail(id, {relations: ["category"]});
};
