import {GraphQLScalarType, Kind} from "graphql";

const minLength = 8;

const isValidPassword = (string) => {
    return true;
    return string.length >= minLength;
}

export const passwordResolver = new GraphQLScalarType({
    name: 'password',
    description: `password string with at least ${minLength} characters`,
    serialize(value) {
        return value;
    },
    parseValue(value) {
        if (isValidPassword(value)) {
            return value;
        }
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                if (isValidPassword(ast.value)) {
                    return ast.value;
                }
        }
    }
});