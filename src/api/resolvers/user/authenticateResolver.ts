import {getManager} from "typeorm";
import {User} from "../../../entities/user";
import {checkPassword} from "../../authentication/hashPassword";
import {AuthenticationError} from 'apollo-server-koa';
import {create} from "domain";
import {createToken} from "../../authentication/token";

export const authenticateResolver = async function (obj, attrs, context, info) {
    const entityManager = getManager();

    const user = await entityManager.findOne(User, {email: attrs.email});

    if (!user) {
        throw new AuthenticationError("user not found");
    }

    if (checkPassword(attrs.password, user.password) !== true) {
        throw new AuthenticationError("wrong password");
    }

    const token = await createToken(user);

    return {
        user: user,
        token: token
    };
};