import {getRepository} from "typeorm";
import {Region} from "../../../entities/region";
import {Event} from "../../../entities/event";
import {Venue} from "../../../entities/venue";
import {Genre} from "../../../entities/genre";

export const badgesResolver = async function (obj, {id, slug}, context, info) {
    let region = obj as Region;

    let eventsCount = getRepository(Event).createQueryBuilder("event")
        .leftJoinAndSelect("event.activeVersion", "activeVersion")
        .where("event.regionId = :regionId", {regionId: region.id})
        .andWhere("activeVersion.isReviewPending = true")
        .getCount();

    let venuesCount = getRepository(Venue).createQueryBuilder("venue")
        .where("venue.region = :regionId", {regionId: region.id})
        .andWhere('venue.isReviewPending = true')
        .getCount();

    let genresCount = getRepository(Genre).createQueryBuilder("genre")
        .innerJoinAndSelect( "genre.category", "category")
        .andWhere("category.region = :regionId", {regionId: region.id})
        .andWhere("genre.isReviewPending = true")
        .getCount();

    return {
        pendingEventsCount: eventsCount,
        pendingVenuesCount: venuesCount,
        pendingGenresCount: genresCount
    };
};