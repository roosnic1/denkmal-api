import {getManager} from "typeorm";
import {AccessLevelEnum, User} from "../entities/user";
import {ApolloServer} from "apollo-server-koa";
import {createTestClient} from "apollo-server-testing";
import {typeDefs} from "../api/typeDefs";
import {resolvers} from "../api/resolvers";
import {connectDatabaseTests} from "../database/database";

export const createTestServer = async function(accessLevel : AccessLevelEnum | undefined = undefined) {
    await connectDatabaseTests();

    const entityManager = getManager();
    let user = null;

    if (accessLevel) {
        user = new User();
        user.name = "test user";
        user.email = "test@test.com";
        user.password = "pw";
        user.accessLevel = accessLevel;
        await entityManager.save(user);
    }

    const apolloServer = new ApolloServer({
        typeDefs,
        resolvers,
        context: () => {
            return {user: user}
        }
    });

    return createTestClient(apolloServer);
};