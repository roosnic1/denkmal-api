import {getConnection} from "typeorm";

require ('newrelic');
require('@newrelic/koa');

import * as Koa from 'koa';
import * as cors from '@koa/cors';
import * as koaRouter from 'koa-router';
import {connectDatabase} from "./database/database";
import {typeDefs} from 'api/typeDefs';
import {resolvers} from 'api/resolvers';
import {ApolloServer} from 'apollo-server-koa';
import * as compress from 'koa-compress';
import {Level, WinstonLogger} from "./logger/logger";
import {apolloAuthenticationContext} from "./api/authentication/apolloAuthenticationContext";

const server = async () => {
    let loggingOptions = {
        console: {level: Level.info},
    };
    if (process.env.ELASTICSEARCH_URL) {
        Object.assign(loggingOptions,  {
            elasticsearch: {
                level: Level.info,
                host: process.env.ELASTICSEARCH_URL
            }
        });
    }
    const logger = new WinstonLogger(
        loggingOptions,{
        program: 'denkmal-api'
    });
    logger.info("starting application");

    await connectDatabase(logger);

    const apolloServer = new ApolloServer({
        typeDefs,
        resolvers,
        formatError: error => {
            logger.warn(`GraphQL error: ${error}`, {error});
            return error;
        },
        formatResponse: response => {
            return response;
        },
        context: apolloAuthenticationContext
    });
    const app = new Koa();
    const router = new koaRouter();
    router.get('/healthz', (ctx, next) => {
        getConnection();
        ctx.body = "";
    });

    app.use(async (ctx, next) => {
        logger.debug(`HTTP request: ${ctx.request.originalUrl}`);
        await next();
    });

    app.use(compress({
        threshold: 1024
    }));

    apolloServer.applyMiddleware({app});

    app.use(router.routes());
    app.use(cors({origin: '*'}));
    app.listen(process.env.PORT || 5000);

    logger.info("listening for requests");
};

server();
