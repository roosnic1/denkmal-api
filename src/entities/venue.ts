import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany, ManyToOne, RelationId, Unique, Index
} from 'typeorm';
import {Event} from "./event";
import {Region} from "./region";
import {IsEmail} from "class-validator";
import {sharedDateTransformer} from "./transformers/dateTransformer";
import moment = require("moment-timezone");
import {VenueAlias} from "./venueAlias";

@Entity('venues')
@Unique(["region", "name"])
export class Venue {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: moment.Moment;

    @UpdateDateColumn({transformer: sharedDateTransformer})
    updatedAt: moment.Moment;

    @Column('text')
    name: string;

    @Column('text', {nullable: true})
    address: string;

    @Column('text', {nullable: true})
    url: string;

    @Column('text', {nullable: true})
    @IsEmail()
    email: string;

    @Column('real', {nullable: true})
    latitude: number;

    @Column('real', {nullable: true})
    longitude: number;

    /*
    formerly called "queued"
     */
    @Column('boolean', {default: false, nullable: false})
    @Index()
    isReviewPending: boolean;

    /*
    suspended venues shouldn't be selectable by the user
     */
    @Column('boolean', {default: false, nullable: false})
    isSuspended: boolean;

    /*
    venue is ignored by scraper
     */
    @Column('boolean', {default: false, nullable: false})
    ignoreScraper: boolean;

    @Column('text', {nullable: true})
    facebookPageId: string;

    @Column('text', {nullable: true})
    twitter: string;

    @OneToMany(type => Event, event => event.venue, {

    })
    events: Promise<Event[]>;

    @ManyToOne(type => Region, region => region.venues, {
        nullable: false,
        onDelete: "CASCADE"
    })
    region: Promise<Region>;

    @RelationId((venue: Venue) => venue.region)
    regionId: string;

    @OneToMany(type => VenueAlias, venueAlias => venueAlias.venue, {
        eager: true,
    })
    aliases: VenueAlias[];
}
