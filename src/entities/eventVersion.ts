import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    Index, OneToOne, ManyToOne, RelationId, OneToMany, ManyToMany, JoinTable
} from 'typeorm';
import {Event} from "./event";
import {Moment} from "moment-timezone";
import {sharedDateTransformer} from "./transformers/dateTransformer";
import * as equal from 'deep-equal';
import {GenreCategory} from "./genreCategory";
import {Genre} from "./genre";


export enum SourceTypeEnum {
    Scraper = "Scraper",
    Suggestion = "Suggestion",
    Admin = "Admin",
    Migration = "Migration"
}

@Entity('eventVersions')
export class EventVersion {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: Moment;

    @CreateDateColumn({transformer: sharedDateTransformer})
    updatedAt: Moment;

    @Column('timestamp', {transformer: sharedDateTransformer})
    @Index()
    from: Moment;

    @Column('timestamp', {nullable: true, transformer: sharedDateTransformer})
    @Index()
    until: Moment;

    @Column('text', {nullable: true})
    description: string;

    /*
    formerly called "queued"
    */
    @Column('boolean', {default: true, nullable: false})
    @Index()
    isReviewPending: boolean;

    @Column('boolean', {default: true, nullable: false})
    hasTime: boolean;

    @ManyToOne(type => Event, event => event.versions, {
        nullable: false,
        onDelete: "CASCADE"
    })
    @Index()
    event: Promise<Event>;

    @RelationId((eventVersion: EventVersion) => eventVersion.event)
    eventId: string;

    @Column("simple-json", {
        nullable: false,
        default: '[]'
    })
    links: [{ label: string, url: string }];

    @ManyToMany(type => Genre, genre => genre.eventVersions, {

    })
    @JoinTable()
    genres: Genre[];

    @Column('text')
    sourceType: SourceTypeEnum;

    @Column('text', {nullable: true})
    sourceIdentifier: string;

    @Column('text', {nullable: true})
    sourceUrl: string;

    @Column('int', {nullable: true})
    sourcePriority: number;

    /*
        comparison ignoring source, save/update dates and review state
     */
    isSame(eventVersion: EventVersion) {
        return eventVersion.eventId == this.eventId
            && eventVersion.description == this.description
            && eventVersion.from.isSame(this.from)
            && ((!eventVersion.until && !this.until) || (eventVersion.until && this.until && eventVersion.until.isSame(this.until)))
            && eventVersion.genres.length === this.genres.length
            && eventVersion.genres.every((g) => this.genres.find(e => e.id === g.id) !== undefined)
            && equal(eventVersion.links, this.links)
    }
}
