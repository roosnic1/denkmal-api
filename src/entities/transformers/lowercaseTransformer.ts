import {ValueTransformer} from "typeorm/decorator/options/ValueTransformer";

export class LowercaseTransformer implements ValueTransformer {
    to(value: String) {
        if (!value) {
            return undefined;
        }
        return value.toLowerCase();
    }

    from(value: any) {
        return value
    }
}

export const sharedLowercaseTransformer = new LowercaseTransformer();