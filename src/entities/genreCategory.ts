import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, Unique, UpdateDateColumn
} from "typeorm";
import {Genre} from "./genre";
import {Region} from "./region";
import moment = require("moment-timezone");
import {sharedDateTransformer} from "./transformers/dateTransformer";
import {sharedLowercaseTransformer} from "./transformers/lowercaseTransformer";

export const unknownGenreCategoryName = "unknown";

@Entity('genreCategories')
@Unique(['region', 'name'])
export class GenreCategory {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: moment.Moment;

    @UpdateDateColumn({transformer: sharedDateTransformer})
    updatedAt: moment.Moment;

    @Column({transformer: sharedLowercaseTransformer})
    @Index()
    name: string;

    @Column({
        nullable: true
    })
    color: string;

    @OneToMany(type => Genre, genre => genre.category, {
        nullable: true
    })
    genres: Genre[];

    @ManyToOne(type => Region, region => region.genreCategories, {
        nullable: false,
        onDelete: "CASCADE"
    })
    @Index()
    region: Promise<Region>;
}