import {Region} from "./region";
import * as moment from 'moment-timezone';

test('getEventDayRange Zurich', () => {
    const region = Object.assign(new Region(), {
        timeZone: "Europe/Zurich",
        dayOffset: 12
    });

    let [start, end] = region.getEventDayRange(moment("2018-12-10"));

    expect(start.unix()).toEqual(moment("2018-12-10T11:00:00Z").unix());
    expect(end.unix()).toEqual(moment("2018-12-11T10:59:59Z").unix());
});

test('getEventDayRange St. Johns', () => {
    const region = Object.assign(new Region(), {
        timeZone: "America/St_Johns",
        dayOffset: 2
    });

    let [start, end] = region.getEventDayRange(moment("2019-06-06"));

    expect(start.unix()).toEqual(moment("2019-06-06T04:30:00Z").unix());
    expect(end.unix()).toEqual(moment("2019-06-07T04:29:59Z").unix());
});

test('getEventDayRangeNow Etc/GMT+12', () => {
    const region = Object.assign(new Region(), {
        timeZone: "Etc/GMT+12",
        dayOffset: 0
    });

    let now = moment.tz(region.timeZone).subtract(region.dayOffset, 'hour');
    let [start, end] = region.getEventDayRangeNow();

    expect(start.unix()).toEqual(now.startOf('day').unix());
    expect(end.unix()).toEqual(now.endOf('day').unix());
});

test('getEventDayRangeIncluding', () => {
    const region = Object.assign(new Region(), {
        timeZone: "Europe/Zurich",
        dayOffset: 5
    });

    {
        let [start, end] = region.getEventDayRangeIncluding(moment.tz("2019-01-31T03:00:00Z", "UTC"));

        expect(start.toISOString()).toEqual("2019-01-30T04:00:00.000Z");
        expect(end.toISOString()).toEqual("2019-01-31T03:59:59.999Z");
    }

    {
        let [start, end] = region.getEventDayRangeIncluding(moment.tz("2019-01-31T04:00:00Z", "UTC"));

        expect(start.toISOString()).toEqual("2019-01-31T04:00:00.000Z");
        expect(end.toISOString()).toEqual("2019-02-01T03:59:59.999Z");
    }

});

test('getEventDay', () => {
    const region = Object.assign(new Region(), {
        timeZone: "Europe/Zurich",
        dayOffset: 5
    });

    expect(region.getEventDay(moment("2030-02-08 08:00:00.000"))).toEqual("2030-02-08");
    expect(region.getEventDay(moment("2030-02-08 04:00:00.000"))).toEqual("2030-02-08");
    expect(region.getEventDay(moment("2030-02-08 03:00:00.000"))).toEqual("2030-02-07");
});
