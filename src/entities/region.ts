import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany, Index
} from 'typeorm';

import {User} from "./user";
import {Venue} from "./venue";
import moment = require("moment-timezone");
import {IsEmail} from "class-validator";
import {sharedDateTransformer} from "./transformers/dateTransformer";
import {GenreCategory} from "./genreCategory";

@Entity('regions')
export class Region {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: moment.Moment;

    @UpdateDateColumn({transformer: sharedDateTransformer})
    updatedAt: moment.Moment;

    @Column('text')
    name: string;

    @Column('text', {nullable: false})
    @IsEmail()
    email: string;

    @Index()
    @Column('text', {unique: true})
    slug: string;

    @Column('real', {nullable: false})
    latitude: number;

    @Column('real', {nullable: false})
    longitude: number;

    @Column('text', {default: "UTC"})
    timeZone: string;

    @Column('real', {default: 5})
    dayOffset: number;

    @Column('timestamp', {nullable: true, transformer: sharedDateTransformer})
    suspendedUntil: moment.Moment;

    @Column('text', {nullable: true})
    facebookPage: string;

    @Column('text', {nullable: true})
    twitterAccount: string;

    @Column('text', {nullable: true})
    footerUrl: string;

    @OneToMany(type => Venue, venue => venue.region, {

    })
    venues: Promise<Venue[]>;

    @OneToMany(type => GenreCategory, genreCategory => genreCategory.region, {

    })
    genreCategories: GenreCategory[];


    @OneToMany(type => User, user => user.region, {

    })
    users: Promise<User[]>;


    getEventDayRangeIncluding(date: moment.Moment) : [moment.Moment, moment.Moment] {
        return this.getEventDayRange(moment.tz(date, this.timeZone).subtract(this.dayOffset, 'hour'));
    }

    getEventDayRangeNow() : [moment.Moment, moment.Moment] {
        return this.getEventDayRangeIncluding(moment.tz(this.timeZone));
    }

    /*
    gets the start and end moments from an eventDay
     */
    getEventDayRange(eventDay: moment.Moment) : [moment.Moment, moment.Moment] {
        const timeZoneDate = moment.tz(this.timeZone)
            .set({
                year: eventDay.year(),
                month: eventDay.month(),
                date: eventDay.date(),
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0});

        const startUtc = moment(timeZoneDate).startOf("day").add(this.dayOffset, 'hour');
        const endUtc = moment(timeZoneDate).endOf("day").add(this.dayOffset, 'hour')

        return [
            startUtc,
            endUtc];
    }

    getEventDay(date: moment.Moment) : string {
        const timeZoneDate = moment.tz(date, this.timeZone);
        timeZoneDate.subtract(this.dayOffset, 'hour');

        return timeZoneDate.format("YYYY-MM-DD");
    }
}
