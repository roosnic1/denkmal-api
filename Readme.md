Denkmal API
===========

Local API development
---------------------
### 1: Run a postgres db
```
docker run -e TZ=UTC -e PGTZ=UTC -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres -p 5432:5432 postgres:alpine
```
### 2: Install dependencies
```
yarn install
```

The `newrelic` package currently requires NodeJS <11.0.
If you're using version 11.0+ and want to ignore that requirement, run:
```
yarn install --ignore-engines
```
or switch to Node 10 with
```
nvm use 10
``` 

### 3: Run nodemon and ts-node
```
yarn start
```

The service runs at loalhost:5000 and nodemon watches for changes in the src directory.


### Run tests
A separate test db needs to run under port 5433
```
docker run -e TZ=UTC -e PGTZ=UTC -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres -p 5433:5432 postgres:alpine
```
Once the database is ready run the tests
```
yarn test
```

### Database migrations
All database migrations are run automatically on application start.
If the schema has been changed, a new migration has to be created:
`yarn typeorm migration:generate -n {migration name}`

### Create new entity
`yarn typeorm entity:create -n {entity name}`
After creating a new entity, the entity needs to be referenced in `ormconfig.yml` for it to be discovered by the database migration generation.
Also, the entity must be added to the database ConnectionOptions inside `database.ts`.

Alternative: Docker execution
-----------------------------
### 1: Build docker images
```
docker-compose build
```
### 2: Start
```
docker-compose up
```


API Endpoint
--------
[http://localhost:5000/graphql](http://localhost:5000/graphql)

GraphQL Playground
--------
Under the same URL as the API endpoint the GraphQL Playground IDE can be shown in a browser
[http://localhost:5000/graphql](http://localhost:5000/graphql)

Load Mock Data
-------
To load mock data into postgres (it will first flush the DB) run the "addMockData" script:
```
yarn run addMockData
```
